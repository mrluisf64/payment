import axios from 'axios'

class AuthService {
    login(credentials) {
        return axios.post("api/auth/login", credentials);
    }

    register(form) {
        return axios.post('api/auth/signup', form);
    }

}

export default new AuthService();
