import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const routes = [
    {
        path: "/store",
        component: require("../components/store/Store").default,
        name: "store"
    },
    {
        path: "/",
        component: require("../components/Home").default,
        name: "auth"
    }
];

export default new Router({
    routes, // short for `routes: routes`
    linkExactActiveClass: "active",
    mode: "hash",
    scrollBehavior() {
        return { x: 0, y: 0 };
    }
});
