import Vue from 'vue'

export function sendNotification(title, text, variant) {
    Vue.notify({
        group: "main",
        type: variant,
        title: title,
        text: text,
    });
}

export const sendNotificationError = () => {
    sendNotification("Ha ocurrido un problema", "Existen errores, por favor, verifique.", 'error');
}

export const sendNotificationSuccess = (title, text) => {
    sendNotification(title, text, 'success');
}
