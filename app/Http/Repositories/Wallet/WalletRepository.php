<?php

namespace App\Http\Repositories\Wallet;

use App\Wallet;
use Illuminate\Http\Request;
use App\Http\Interfaces\Wallet\WalletInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class WalletRepository implements WalletInterface
{

    use AuthorizesRequests;

    private $model;

    public function __construct(Wallet $wallet)
    {
        $this->model = $wallet;
    }

    public function getWallet()
    {
        $wallet = $this->model->with('user')
            ->where('user_id', auth()->user()->id)
            ->first();
        return response()->json($wallet, 200);
    }

    public function getWalletById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function consultBalanceWallet(Request $request)
    {
        $wallet = auth()->user()->wallet;

        $this->authorize('view', [$wallet, $request]);

        return response()->json([
            'message' => 'Consult balance successfully!',
            'json' => $wallet
        ], 200);
    }

    public function rechargeWallet(Request $request, $id)
    {
        $wallet = $this->getWalletById($id);

        $this->authorize('view', [$wallet, $request]);

        $wallet->update([
            'balance' => $wallet->balance + $request->value,
        ]);

        $wallet->user->transactions()->create([
            'detail' => 'Recharge Wallet',
            'mount' => $request->value,
            'state' => 'success',
            'token' => null,
            'session_id' => $request->session()->get('_token')
        ]);

        return response()->json([
            'message' => 'Recharge successfully!',
            'json' => $wallet
        ], 200);
    }
}
