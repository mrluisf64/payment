<?php

namespace App\Http\Controllers\Transactions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repositories\Wallet\WalletRepository;
use App\Http\Requests\Wallet\ConsultBalanceRequest;
use App\Http\Requests\Transactions\RechargeWalletRequest;

class WalletController extends Controller
{
    private $repository;

    public function __construct(WalletRepository $wallet){
        $this->repository = $wallet;
    }

    public function index() {
       return $this->repository->getWallet();
    }

    public function store(ConsultBalanceRequest $request) {
        return $this->repository->consultBalanceWallet($request);
    }

    public function update(RechargeWalletRequest $request, $id) {
        return $this->repository->rechargeWallet($request, $id);
    }

}
