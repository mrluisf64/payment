<?php

namespace App\Http\Interfaces\Payment;

use Illuminate\Http\Request;
use App\Http\Requests\Payment\PaymentConfirmRequest;

interface PaymentInterface
{
    public function generateEmail(PaymentConfirmRequest $request);
    public function confirmPayment(Request $request);
}
